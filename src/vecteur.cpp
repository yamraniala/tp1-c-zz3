#include "vecteur.hpp"




Vecteur::Vecteur()
{
    tab = new int[10];
    count = 0;

    if(tab != nullptr)
    {
        taille = 10;
        std::cout << "la taille de ce vecteur est : " << taille << std::endl;

        for(int i = 0; i < taille; i++) tab[i] = 0;
    }
}


Vecteur::Vecteur(int taille)
{
    tab = new int[taille];
    this->taille = taille;
}

Vecteur::Vecteur(const Vecteur& v)
{
    this->tab = new int[v.getTaille()];
}

int Vecteur::getCount() const
{
    return count;
}



int Vecteur::operator[](int index) const
{
    int result = 9999;

    if(tab != nullptr && (index < count) && (index >=0)) result = tab[index];

    return result;
}


std::ostream& operator<<(std::ostream& flux,const Vecteur& v) 
{
    for(int i=0; i<v.getCount(); i++)
    {
        flux << v[i] << std::endl;
    }

    return flux;
}

void Vecteur::ajouter(int nv)
{
    //std::cout << count << std::endl;
    tab[count++] = nv;
}


int Vecteur::getTaille() const
{
    return taille;
}
