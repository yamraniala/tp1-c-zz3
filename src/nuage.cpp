#include "nuage.hpp"



void Nuage::ajouter(Point& p)
{
    tab.push_back(&p);
}


int Nuage::size()
{
    return tab.size();
}


Nuage::const_iterator Nuage::begin()
{
    return tab.begin();
}

Nuage::const_iterator Nuage::end()
{
    return tab.end();
}

Cartesien barycentre(Nuage n)
{
    double sommeX;
    double sommeY;

    for(auto it = n.begin(); it < n.end(); it++) 
    {
        sommeX += ((Cartesien *) *it)->getX();
        sommeY += ((Cartesien *) *it)->getY();
    }

    Cartesien result(sommeX/n.size(), sommeY/n.size());

    return result;
}


Cartesien BarycentreCartesien::operator()(const Nuage& n)
{
    return barycentre(n);
}


Polaire BarycentrePolaire::operator()(const Nuage& n)
{
    Polaire p;
    BarycentreCartesien()(n).convertir(p);

    return p;
}