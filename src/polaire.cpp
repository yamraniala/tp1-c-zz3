#include "polaire.hpp"

Polaire::Polaire() : r(0), tetha(0)
{}

Polaire::Polaire(double a, double r): r(r), tetha(a)
{

}



void Polaire::afficher(std::ostream & flux) const
{
    flux<< "(a=" << tetha << ";d=" << r << ")";
}


double Polaire::getAngle() const 
{
    return tetha;
}


double Polaire::getDistance() const 
{
    return r;
}


void Polaire::setAngle(double const a)  
{
    this->tetha = a;
}


void Polaire::setDistance(double const r)  
{
    this->r = r;
}

void Polaire::convertir(Polaire & p) const
{
    p.setAngle(this->getAngle());
    p.setDistance(this->getDistance());
}

void Polaire::convertir(Cartesien & p) const
{
    p.setX(this->getDistance() * std::cos(this->getAngle() * M_PI / 180));
    p.setY(this->getDistance() * std::sin(this->getAngle() * M_PI / 180));
}


Polaire::Polaire(const Cartesien& c)
{
    c.convertir(*this);
}