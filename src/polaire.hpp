#ifndef ZZ3
#define ZZ3

#include <iostream>
#include "point.hpp"

#include "cartesien.hpp"


class Polaire : public Point
{

    double r;
    double tetha;


public:

    Polaire();
    Polaire(double, double);
    void afficher(std::ostream & ) const override;
    double getAngle() const;
    double getDistance() const;
    void setAngle(double const ) ;
    void setDistance(double const ) ;
    void convertir(Polaire &) const override;
    void convertir(Cartesien &) const override;

    Polaire(const Cartesien&); // ~~ constructeur de copie
};




#endif


