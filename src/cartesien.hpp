#ifndef AP
#define AP

#include <iostream>
#include "point.hpp"

#include "polaire.hpp"



class Cartesien : public Point
{


    double x;
    double y;


public :


    Cartesien(double r =0 , double yy =0);
    void afficher(std::ostream&) const override;
    double getX() const;
    double getY() const;
    void setX(double);
    void setY(double);
    void convertir(Polaire&) const override;
    void convertir(Cartesien &) const override;

    Cartesien(const Polaire&); // ~~ constructeur de copie
};




#endif