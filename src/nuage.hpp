#ifndef NUAGE_TP1
#define NUAGE_TP1


#include <vector>

#include "cartesien.hpp"

class Point;
class const_iterator;
class Cartesien;
class Polaire;

class Nuage  
{
    std::vector<Point *> tab;

public :

    using const_iterator = std::vector<Point *>::const_iterator;

    void ajouter(Point& );
    int size();
    const_iterator begin();
    const_iterator end();


};

Cartesien barycentre(Nuage);

class BarycentreCartesien
{

public:
    Cartesien operator()(const Nuage& );
};


class BarycentrePolaire
{

public :
    Polaire operator()(const Nuage& );


};



#endif