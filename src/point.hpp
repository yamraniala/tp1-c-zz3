#ifndef TP1
#define TP1

#include <sstream>
#include <cmath>
#include <vector>



using namespace std;

class Polaire;  //declaration anticipe pr empecher l'inclusion circulaire
class Cartesien; //meme chose


//classe abstraite : elle au moin une methode abstraite
class Point 
{



public : 


    virtual void afficher(std::ostream& flux) const = 0 ;  //methode abstraite : virtuelle pure
    virtual void convertir(Polaire &) const = 0;
    virtual void convertir(Cartesien &) const = 0;


};


ostream& operator<<(ostream& flux, const Point& p);


#endif