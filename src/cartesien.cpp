#include "cartesien.hpp"


void Cartesien::afficher(std::ostream& flux) const
{
    flux<< "(x=" << x << ";y=" << y << ")";
}


double Cartesien::getX() const
{
    return x;
}

double Cartesien::getY() const
{
    return y;
}

void Cartesien::setX(double x)
{
    this->x = x;
}

void Cartesien::setY(double y)
{
    this->y = y;
    
}


Cartesien::Cartesien(double x , double y) : x(x), y(y)
{}



void Cartesien::convertir(Polaire& p) const
{
    p.setAngle(std::atan2(y, x) * 180 / M_PI);
    p.setDistance(std::hypot(x, y));
}


void Cartesien::convertir(Cartesien& p) const
{
    p.setX(this->getX());
    p.setY(this->getY());
}

Cartesien::Cartesien(const Polaire& p)
{
    p.convertir(*this);
}