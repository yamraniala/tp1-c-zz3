#ifndef TP1_VECTEUR_HPP
#define TP1_VECTEUR_HPP

#include <iostream>


class Vecteur 
{

    int * tab;
    int count;
    int taille;

public:
    Vecteur();
    Vecteur(int);
    Vecteur(const Vecteur&);
    int getCount() const;
    int getTaille() const;
    
    int operator[](int ) const;

    void ajouter(int);
    


};

std::ostream& operator<<(std::ostream&,const Vecteur&) ;


#endif